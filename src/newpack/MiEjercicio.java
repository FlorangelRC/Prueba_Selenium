package newpack;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebElement;


class xappia{
	String tipo;
	int pag, pos;
	xappia(String tipo, int pag, int pos){
		this.tipo=tipo;
		this.pag=pag;
		this.pos=pos;
	}
	void mostrarXap(int i){
		System.out.println(i+")Tipo: "+tipo+" | Pagina: "+pag+" | Posicion: "+pos);
	}
}

public class MiEjercicio {
	
	//buscar y mostrar anuncios
	static void verAnuncios (WebDriver driver)
	{
		List<WebElement> anuncios= driver.findElement(By.id("tads")).findElements(By.tagName("a"));//anuncios iniciales
		anuncios.addAll(driver.findElement(By.id("tadsb")).findElements(By.tagName("a"))); //anuncios finales
		System.out.println("\n\n***ANUNCIOS***");
		for (WebElement an : anuncios)
	    {
			if (an.getAttribute("data-preconnect-urls")!=null){
				System.out.println(an.getText());
				System.out.println((an.getAttribute("data-preconnect-urls").split(","))[0]+"\n");
			}
	    }
	}

	//mostrar las paginas que contengan .ar
	static void mostrarAr(WebDriver driver, int i){
		List<WebElement> pagsArg= driver.findElements(By.xpath("//*[@id='rso']//h3/a"));;
		System.out.println("\n-Pagina de resultado N°"+i);
		for (WebElement resultados : pagsArg )
	    {
		 if (resultados.getAttribute("href").contains(".ar"))
	        System.out.println(resultados.getAttribute("href"));
	    }
		
	}
	
	//almacenar los datos de las paginas que contienen xappia.com
	static void porXappia(WebDriver driver, int i, List<xappia> pagsXa){
		List<WebElement> todas= driver.findElements(By.xpath("//*[@id='center_col']//h3/a"));
		int pos = 0;
		for (WebElement resultados : todas )
		{
			if(resultados.getAttribute("data-preconnect-urls")!=null){
				pos++;
				if (resultados.getAttribute("data-preconnect-urls").contains("xappia.com"))
					pagsXa.add(new xappia("Publicidad", i, todas.indexOf(resultados)-pos+1));	
			}else if (resultados.getAttribute("href").contains("xappia.com"))
				pagsXa.add(new xappia("Organica", i, todas.indexOf(resultados)-pos+1));	
		}
	}
	
	//mostrar todas las paginas que contienen xappia.com
	static void mostrarXa(List<xappia> pagsXa){
		for (xappia uno : pagsXa )
		{
			uno.mostrarXap(pagsXa.indexOf(uno)+1);
		}
	}
	
	public static void main(String[] args) {
		List<xappia> pagsXa = new ArrayList<xappia>();
		System.setProperty("webdriver.chrome.driver", "../selenium/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.get("http://google.com");
		
		//ingresar el texto en la caja de busqueda de google
		driver.findElement(By.id("lst-ib")).sendKeys("Salesforce Argentina");
				
		//envia la busqueda
		driver.findElement(By.id("lst-ib")).submit();
		
		//espera hasta que la pagina cargue
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		
		//*********************ANUNCIOS INICIO************
		verAnuncios(driver);
		//*********************ANUNCIOS FIN************
		porXappia(driver, 1, pagsXa);
		
		//*********************PAGINAS ARGENTINAS INICIO*********************
		System.out.println("\n***PAGINAS ARGENTINAS***");
		mostrarAr(driver,1);
		List<WebElement> siguientes= driver.findElements(By.xpath("//*[@id='nav']//a")).subList(0, 4);
		String[] direcciones = new String[4];
		for (int i = 0; i<=3; i++ )
	    {
			direcciones[i]=siguientes.get(i).getAttribute("href");
	    }
		for (int i = 0; i<=3; i++)
	    {
			driver.navigate().to(direcciones[i]);
			porXappia(driver, i+2, pagsXa);
			driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
			mostrarAr(driver,i+2);
	    }
		
		//*********************PAGINAS ARGENTINAS FIN************************
		
		//************************XAPPIA.COM INICIO**************************
		System.out.println("\n\n***DETALLES DE APARICION DE XAPPIA.COM***");
		mostrarXa(pagsXa);
		
		//*************************XAPPIA.COM FIN****************************
	}

}


